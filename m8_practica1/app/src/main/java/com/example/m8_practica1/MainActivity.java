package com.example.m8_practica1;

import android.os.Bundle;


import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;


import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int DEFAULT_MSG_LENGTH_LIMIT = 1000;

    private ListView mMessageListView;
    private PreguntaAdapter mPreguntaAdapter;
    private ProgressBar mProgressBar;
    private EditText mMessageEditText;
    private Button mSendButton;
    private EditText resposta1,resposta2,resposta3,resposta4,longitud,latitud,real;




    // Firebase instance variables
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mMessagesDatabaseReference;
    private ChildEventListener mChildEventListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        // Initialize Firebase components
        mFirebaseDatabase = FirebaseDatabase.getInstance();


        mMessagesDatabaseReference = mFirebaseDatabase.getReference().child("preguntes");


        // Initialize references to views
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mMessageListView = (ListView) findViewById(R.id.messageListView);

        mMessageEditText = (EditText) findViewById(R.id.messageEditText);
        resposta1 = (EditText) findViewById(R.id.edresposta1);
        resposta2 = (EditText) findViewById(R.id.edresposta2);
        resposta3 = (EditText) findViewById(R.id.edresposta3);
        resposta4 = (EditText) findViewById(R.id.edresposta4);
        longitud = (EditText) findViewById(R.id.edlongitud);
        latitud = (EditText) findViewById(R.id.edlatitud);
        real = (EditText) findViewById(R.id.edrespostaReal);
        mSendButton = (Button) findViewById(R.id.sendButton);

        // Initialize message ListView and its adapter
        List<pregunta> preguntas = new ArrayList<>();
        mPreguntaAdapter = new PreguntaAdapter(this, R.layout.item_message, preguntas);
        mMessageListView.setAdapter(mPreguntaAdapter);

        // Initialize progress bar
        mProgressBar.setVisibility(ProgressBar.INVISIBLE);



        // Enable Send button when there's text to send
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        mMessageEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(DEFAULT_MSG_LENGTH_LIMIT)});

        // Send button sends a message and clears the EditText
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pregunta pregunta = new pregunta(mMessageEditText.getText().toString(), resposta1.getText().toString(),resposta2.getText().toString(),
                        resposta3.getText().toString(),resposta4.getText().toString(),Double.parseDouble(longitud.getText().toString()),
                        Double.parseDouble(latitud.getText().toString()),real.getText().toString());
                mMessagesDatabaseReference.push().setValue(pregunta);

                attachDatabaseReadListener();


                // Clear input box
                mMessageEditText.setText("");
                resposta1.setText("");
                resposta2.setText("");
                resposta3.setText("");
                resposta4.setText("");
                longitud.setText("");
                latitud.setText("");
                real.setText("");
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void attachDatabaseReadListener() {
        if (mChildEventListener == null) {
            mChildEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    pregunta pregunta = dataSnapshot.getValue(pregunta.class);
                    mPreguntaAdapter.add(pregunta);
                }

                public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
                public void onChildRemoved(DataSnapshot dataSnapshot) {}
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
                public void onCancelled(DatabaseError databaseError) {}
            };
            mMessagesDatabaseReference.addChildEventListener(mChildEventListener);
        }
    }


}
